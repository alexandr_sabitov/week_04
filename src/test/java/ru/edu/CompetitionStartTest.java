package ru.edu;

import org.junit.Before;
import org.junit.Test;
import ru.edu.model.*;

import java.util.List;

import static org.junit.Assert.*;

public class CompetitionStartTest {
    Participant participant1;
    Participant participant2;
    Competition competition = new CompetitionStart();
    CountryParticipant country;

    @Before
    public void setUp() throws Exception {

        participant1 = competition.register(new AthleteImpl("Ivan", "Petrov", "RUSSIA"));
        assertEquals(1,competition.getResults().size());
        participant2 = competition.register(new AthleteImpl("Denis", "Petrov", "Poland"));
        assertEquals(2,competition.getResults().size());
    }

    @Test(expected = IllegalArgumentException.class)
    public void registerDuplicate() {
        participant1 = competition.register(new AthleteImpl("Ivan", "Petrov", "RUSSIA"));
        participant1 = competition.register(new AthleteImpl("Ivan", "Petrov", "RUSSIA"));
    }

    @Test
    public void register() {
        Participant participant3 = competition.register(new AthleteImpl("Tom", "Petrov", "GDR"));
        assertEquals(3,competition.getResults().size());
    }

    @Test
    public void updateScore() {
        competition.updateScore(participant1,50L);
        assertEquals(50,participant1.getScore());
        competition.updateScore(participant1.getId(),40L);
        assertEquals(90,participant1.getScore());
        competition.updateScore(1,10L);
        assertEquals(100,participant1.getScore());
    }

    @Test
    public void getResults() {
        Competition competition = new CompetitionStart();
        participant1 = competition.register(new AthleteImpl("Petr", "Ivanov", "RUSSIA"));
        participant2 = competition.register(new AthleteImpl("Petr", "Petrov", "Poland"));
        competition.updateScore(participant1,50L);
        competition.updateScore(participant2,40L);
        List<Participant> participants = competition.getResults();

        assertEquals(participant1,participants.get(0));
        assertEquals(participant2,participants.get(1));

    }

    @Test
    public void getParticipantsCountries() {
        Competition competition = new CompetitionStart();
        Participant participant1 = competition.register(new AthleteImpl("Alex", "Ivanov", "RUSSIA"));
        Participant participant2 = competition.register(new AthleteImpl("Petr", "li", "Poland"));
        Participant participant3 = competition.register(new AthleteImpl("Ivan", "Petrov", "RUSSIA"));
        Participant participant4 = competition.register(new AthleteImpl("Denis", "Sidorov", "Poland"));

        competition.updateScore(participant1.getId(), 120);
        competition.updateScore(participant2.getId(), 10);
        competition.updateScore(participant3.getId(), 30);
        competition.updateScore(participant4.getId(), 50);

        List<CountryParticipant> countryParticipants = competition.getParticipantsCountries();
        assertEquals("RUSSIA",countryParticipants.get(0).getName());
        assertEquals("Poland",countryParticipants.get(1).getName());
    }
}