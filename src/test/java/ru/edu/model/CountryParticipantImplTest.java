package ru.edu.model;

import org.junit.Before;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.*;

public class CountryParticipantImplTest {
    private List<Participant> participants = new LinkedList<>();
    CountryParticipant country;

    @Before
    public void setUp() throws Exception {
        Participant participant = new ParticipantImpl(1,
                (new AthleteImpl("Ivan", "Petrov", "RUSSIA")));
        participants.add(participant);
        country = new CountryParticipantImpl(participant.getAthlete().getCountry(), participants, 10);
    }



    @Test
    public void getName() {
        assertEquals("RUSSIA", country.getName());
    }

    @Test
    public void getParticipants() {
        assertEquals(participants, country.getParticipants());
    }

    @Test
    public void getScore() {
        assertEquals(10L,country.getScore());
    }
}