package ru.edu.model;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

public class AthleteImplTest {
    private Athlete athlete;

    @Before
    public void setUp() throws Exception {
        athlete = new AthleteImpl("Ivan", "Petrov", "RUSSIA");
    }

    @Test
    public void getFirstName() {
        assertEquals("Ivan", athlete.getFirstName());
    }

    @Test
    public void getLastName() {
        assertEquals("Petrov", athlete.getLastName());
    }

    @Test
    public void getCountry() {
        assertEquals("RUSSIA", athlete.getCountry());
    }

    @Test
    public void testEquals() {
        assertEquals(athlete, new AthleteImpl("Ivan", "Petrov", "RUSSIA"));
        assertEquals(athlete, new AthleteImpl("IVAN", "PETROV", "RUSSIA"));
        assertNotEquals(athlete, new AthleteImpl("Ivan", "Petro", "RUSSIA"));
        assertNotEquals(athlete, null);
        assertEquals(athlete,athlete);
    }

    @Test
    public void testHashCode() {
        assertEquals(athlete.hashCode(), new AthleteImpl("Ivan", "Petrov", "RUSSIA").hashCode());
        assertNotEquals(athlete.hashCode(), new AthleteImpl("Ivan", "Petro", "RUSSIA").hashCode());
    }
}