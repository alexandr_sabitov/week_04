package ru.edu.model;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ParticipantImplTest {
    private ParticipantImpl participant;
    private Athlete athlete;

    @Before
    public void setUp() throws Exception {
        athlete = new AthleteImpl("Ivan", "Petrov", "RUSSIA");
        participant = new ParticipantImpl(1, athlete);
    }

    @Test
    public void updateScore() {
    }

    @Test
    public void getId() {
        assertEquals(new Long(1),participant.getId());
    }

    @Test
    public void getAthlete() {
        assertEquals(athlete, participant.getAthlete());
    }

    @Test
    public void getScore() {
        assertEquals(0, participant.getScore());
    }

    @Test
    public void testEquals() {
        assertEquals(participant, new ParticipantImpl(1,athlete));
        assertNotEquals(participant, 1);
        assertNotEquals(participant, null);
        assertEquals(participant,participant);
    }

    @Test
    public void testHashCode() {
        assertEquals(participant.hashCode(), new ParticipantImpl(1,athlete).hashCode());
        assertNotEquals(participant.hashCode(), new ParticipantImpl(2,athlete).hashCode());
    }
}