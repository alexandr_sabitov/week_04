package ru.edu.model;

import java.util.Comparator;
import java.util.Objects;

public class ParticipantImpl implements Participant{
    private final long id;
    private Athlete athlete;
    private long score;

    public ParticipantImpl(final long id, final Athlete athlete) {
        this.id = id;
        this.athlete = athlete;
        this.score = 0;
    }

    public void updateScore(long score) {
        this.score += score;
    }

    /**
     * Получение информации о регистрационном номере.
     *
     * @return регистрационный номер
     */
    @Override
    public Long getId() {
        return id;
    }

    /**
     * Информация о спортсмене
     *
     * @return объект спортсмена
     */
    @Override
    public Athlete getAthlete() {
        return athlete;
    }

    /**
     * Счет участника.
     *
     * @return счет
     */
    @Override
    public long getScore() {
        return score;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ParticipantImpl that = (ParticipantImpl) o;
        return athlete.equals(that.athlete);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, athlete, score);
    }


}
