package ru.edu.model;

import java.util.List;

public class CountryParticipantImpl implements CountryParticipant{
    private String name;
    private List<Participant> participants;
    private long score;

    public CountryParticipantImpl(final String name,
                                  final List<Participant> participants,
                                  final long score) {
        this.name = name;
        this.participants = participants;
        this.score = score;
    }

    /**
     * Название страны.
     *
     * @return название
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * Список участников от страны.
     *
     * @return список участников
     */
    @Override
    public List<Participant> getParticipants() {
        return participants;
    }

    /**
     * Счет страны.
     *
     * @return счет
     */
    @Override
    public long getScore() {
        return score;
    }
}
