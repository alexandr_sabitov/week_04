package ru.edu.model;

import java.util.Locale;
import java.util.Objects;

public class AthleteImpl implements Athlete{
    private String firstName;
    private String lastName;
    private String country;

    public AthleteImpl(final String firstName, final String lastName, final String country) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.country = country;
    }

    /**
     * Имя.
     *
     * @return значение
     */
    @Override
    public String getFirstName() {
        return firstName;
    }

    /**
     * Фамилия.
     *
     * @return значение
     */
    @Override
    public String getLastName() {
        return lastName;
    }

    /**
     * Страна.
     *
     * @return значение
     */
    @Override
    public String getCountry() {
        return country;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AthleteImpl athlete = (AthleteImpl) o;
        return Objects.equals(firstName.toLowerCase(Locale.ROOT),
                athlete.firstName.toLowerCase(Locale.ROOT)) &&
                Objects.equals(lastName.toLowerCase(Locale.ROOT),
                        athlete.lastName.toLowerCase(Locale.ROOT)) &&
                Objects.equals(country.toLowerCase(Locale.ROOT),
                        athlete.country.toLowerCase(Locale.ROOT));
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName.toLowerCase(Locale.ROOT),
                lastName.toLowerCase(Locale.ROOT),
                country.toLowerCase(Locale.ROOT));
    }
}
