package ru.edu;


import ru.edu.model.*;

import java.util.*;

public class CompetitionStart implements Competition {

    Map<Long, ParticipantImpl> participants = new HashMap<>();
    private Long id = 1L;


    /**
     * Регистрация участника.
     *
     * @param participant - участник
     * @return зарегистрированный участник
     * @throws IllegalArgumentException - при попытке повторной регистрации
     */
    @Override
    public Participant register(Athlete participant) {
        ParticipantImpl participantReg = new ParticipantImpl(id, participant);
        if (participants.containsValue(participantReg)) {
            throw new IllegalArgumentException("Данный участник уже зарегистрирован!");
        }
        participants.put(id, participantReg);
        id++;
        return participantReg;
    }

    /**
     * Обновление счета участника по его id.
     * Требуется константное время выполнения
     * <p>
     * updateScore(10) прибавляет 10 очков
     * updateScore(-5) отнимает 5 очков
     *
     * @param id    регистрационный номер участника
     * @param score +/- величина изменения счета
     */
    @Override
    public void updateScore(long id, long score) {
        ParticipantImpl participant = participants.get(id);
        if (participant != null) {
            participant.updateScore(score);
        }
    }

    /**
     * Обновление счета участника по его объекту Participant
     * Требуется константное время выполнения
     *
     * @param participant зарегистрированный участник
     * @param score       новое значение счета
     */
    @Override
    public void updateScore(Participant participant, long score) {
        updateScore(participant.getId(), score);
    }

    /**
     * Получение результатов.
     * Сортировка участников от большего счета к меньшему.
     *
     * @return отсортированный список участников
     */
    @Override
    public List<Participant> getResults() {
        List<Participant> participants = new LinkedList<>(this.participants.values());
        participants.sort(cmpRev);
        return participants;
    }

    /**
     * Получение результатов по странам.
     * Группировка участников из одной страны и сумма их счетов.
     * Сортировка результатов от большего счета к меньшему.
     *
     * @return отсортированный список стран-участников
     */
    @Override
    public List<CountryParticipant> getParticipantsCountries() {
        List<CountryParticipant> resultCountry = new LinkedList<>();

        Map<String, List<Participant>> participantsByCountry = new TreeMap<>();
        Map<String, Long> scoreByCountry = new TreeMap<>();

        for (Participant participant : participants.values()) {
            String country = participant.getAthlete().getCountry();
            if (!participantsByCountry.containsKey(country)) {
                participantsByCountry.put(country,new LinkedList<>());
            }
            participantsByCountry.get(country).add(participant);
            scoreByCountry.put(country,scoreByCountry.getOrDefault(country,0L)
                    + participant.getScore());
        }
        for (Map.Entry<String, List<Participant>> e : participantsByCountry.entrySet()) {
            String country = e.getKey();
            List<Participant> participants = e.getValue();
            resultCountry.add(new CountryParticipantImpl(country,
                    participants,
                    scoreByCountry.get(country)));
        }

        resultCountry.sort(Comparator.comparing(
                CountryParticipant::getScore,Comparator.reverseOrder()));

        return resultCountry;
    }

    /**
     * Компаратор, осуществяющий упорядочивание значений от большего
     * к меньшему.
     *
     */
    Comparator<Participant> cmpRev = (Participant lhs, Participant rhs) ->{
        if (lhs.equals(rhs)) {
            return 0;
        }
        if (lhs.getScore() < rhs.getScore()) {
            return 1;
        } else {
            return -1;
        }

    };
}
